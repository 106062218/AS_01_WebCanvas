window.onload=function(){
	init();
}

var sizeElement=document.getElementById('size');
var opacityElement=document.getElementById('opacity');
var colorElement=document.getElementById('color');
var btns=document.querySelectorAll('button');
var canvas=document.getElementById('myCanvas');
var ctx=canvas.getContext('2d');

var painting = document.getElementById('canvasDiv');
var paint_style = getComputedStyle(painting);
canvas.width = parseInt(paint_style.getPropertyValue('width'));
canvas.height = parseInt(paint_style.getPropertyValue('height'));
var mouse={x: 0, y: 0, start_x: 0, start_y: 0};

var hexNumber = '#000000';


ctx.lineJoin = 'round';
ctx.lineCap = 'round';

var lastClick = btns.length;
var Click = 0;
var pics = new Array();
var step = 0;
var imageLoad = document.getElementById('upload');

function init(){
	ctx.lineWidth=sizeElement.innerHTML;
	ctx.strokeStyle=colorElement.innerHTML;

	imageLoad.addEventListener('change', function(){
		var img = new Image();
		img.src = URL.createObjectURL(imageLoad.files[0]);
		img.onerror = function(){
			alert('unable to load image');
		}
		img.onload = function(){
			ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
		};
	});
	for(var i=0; i<btns.length; i++){
		btns[i].addEventListener('mouseover', (function(i){
			return function(){
				btns[i].style.backgroundColor='hsl(0, 46%, 73%)';
				btns[i].style.color='black';
			}
		})(i));
		btns[i].addEventListener('mouseout', (function(i){
			return function(){
				if(i!==Click || i>=7){
					btns[i].style.background='hsl(0, 46%, 78%)';
					btns[i].style.color='hsl(0, 0%, 25%)';
				}
			}
		})(i));
		btns[i].addEventListener('click', (function(i){
			return function(){
				lastClick=Click;
				Click=i;
				if(lastClick===btns.length){
					lastClick=i;
				}
				btns[lastClick].style.backgroundColor='hsl(0, 46%, 78%)';
				btns[lastClick].style.color='hsl(0, 0%, 25%)';
				btns[Click].style.backgroundColor='hsl(0, 46%, 73%)';
				btns[Click].style.color='black';


				if(Click===7){
					ctx.clearRect(0, 0, canvas.width, canvas.height);
					step++;
					pics.push(canvas.toDataURL());
					if(pics.length>step) step=pics.length;
				}
				else if(Click===8 && step>1){
					step--;
					var pic = new Image();
					pic.src = pics[step-1];
					pic.onload = function(){
						ctx.clearRect(0, 0, canvas.width, canvas.height);
						ctx.drawImage(pic, 0, 0);
					}
				}
				else if(Click===8 && step===1){
					step--;
					ctx.clearRect(0, 0, canvas.width, canvas.height);
				}
				else if(Click===9 && step<pics.length){
					step++;
					var pic=new Image();
					pic.src = pics[step-1];
					pic.onload = function(){
						ctx.clearRect(0, 0, canvas.width, canvas.height);
						ctx.drawImage(pic, 0, 0);
					}
				}
				else if(Click===10){
					document.getElementById('upload').click();
				}
				else if(Click===11){
					var download = document.getElementById("download");
					download.href = canvas.toDataURL();
					download.click();
				}
			}
		})(i));
	}

	document.getElementById('size-slider').addEventListener('mousemove', function(){
		sizeElement.innerHTML=this.value;
		ctx.lineWidth=this.value;
	});

	document.getElementById('opacity-slider').addEventListener('mousemove', function(){
		opacityElement.innerHTML=(this.value)/10;
		ctx.globalAlpha = parseFloat(opacityElement.innerHTML);
		// colorElement.innerHTML = convertHex(hexNumber, this.value);

	});

	document.getElementById('color-picker').addEventListener('change', function(){
		hexNumber = this.value;
		// colorElement.innerHTML = convertHex(this.value, opacityElement.innerHTML + '0');
		// ctx.strokeStyle = colorElement.innerHTML;
		// ctx.fillStyle = colorElement.innerHTML;
		colorElement.innerHTML = this.value;
		ctx.strokeStyle = colorElement.innerHTML;
	});



	canvas.addEventListener('mousemove', function(e) {
		mouse.x = e.pageX - this.offsetLeft;
		mouse.y = e.pageY - this.offsetTop;

		if(Click===0) painting.style.cursor = 'url(./image/pen.png), auto';
		else if(Click===1) painting.style.cursor = 'url(./image/eraser.png), auto';
		else painting.style.cursor = 'initial';
	}, false);
	canvas.addEventListener('mousedown', function(e) {
		mouse.start_x = mouse.x;
		mouse.start_y = mouse.y;

		ctx.beginPath();
		
		if(Click<2){
			ctx.moveTo(mouse.x, mouse.y);
			canvas.addEventListener('mousemove', onPaint, false);
		}
		else if(Click===2){
			starting_pic=ctx.getImageData(0, 0, canvas.width, canvas.height);
			canvas.addEventListener('mousemove', drawCircle, false);
		}
		else if(Click===3){
			starting_pic=ctx.getImageData(0, 0, canvas.width, canvas.height);
			canvas.addEventListener('mousemove', createText, false);
		}
		else if(Click===4){
			starting_pic=ctx.getImageData(0, 0, canvas.width, canvas.height);
			canvas.addEventListener('mousemove', drawRectangle, false);
		}
		else if(Click===5){
			starting_pic=ctx.getImageData(0, 0, canvas.width, canvas.height);
			canvas.addEventListener('mousemove', drawLine, false);
		}
		else if(Click===6){
			starting_pic=ctx.getImageData(0, 0, canvas.width, canvas.height);
			canvas.addEventListener('mousemove', drawTriangle, false);
		}
	}, false);
	 
	canvas.addEventListener('mouseup', function() {
		canvas.removeEventListener('mousemove', onPaint, false);
		canvas.removeEventListener('mousemove', drawLine, false);
		canvas.removeEventListener('mousemove', drawRectangle, false);
		canvas.removeEventListener('mousemove', drawTriangle, false);
		canvas.removeEventListener('mousemove', drawCircle, false);
		canvas.removeEventListener('mousemove', createText, false);
		if(Click===3 && mouse.x!==mouse.start_x){
			ctx.putImageData(starting_pic, 0, 0);
			var font_size = parseInt(sizeElement.innerHTML);
			var font_color = colorElement.innerHTML;
			var box_width = Math.abs(mouse.x - mouse.start_x);
			var box_height = Math.abs(mouse.y - mouse.start_y);
			var input = new CanvasInput({
				canvas: document.getElementById('myCanvas'),
				fontSize: font_size,
				fontFamily: 'Arial',
				fontColor: font_color,
				fontWeight: 'bold',
				width: box_width,
				height: box_height,
				x: mouse.start_x,
				y: mouse.start_y,
				// padding: 8,
				borderWidth: 1,
				borderColor: 'rgba(0, 0, 0, 0)',
				borderRadius: 3,
				boxShadow: '1px 1px 0px rgba(0, 0, 0, 0)',
				innerShadow: '0px rgba(0, 0, 0, 0)',
				placeHolder: 'Enter message here...',
				backgroundColor: 'rgba(255, 255, 255, 0)',
				onsubmit(){
					ctx.font = sizeElement.innerHTML + 'Arial';
					ctx.fontColor = colorElement.innerHTML;
					// ctx.lineWidth = sizeElement.innerHTML;
					// ctx.strokeStyle = colorElement.innerHTML;
					// ctx.fillText(this.selectText(), mouse.start_x, mouse.start_y);
					// this.borderColor = 'rgba(0, 0, 0, 0)';
					// this.borderWidth = '0';

					this.blur();
					this.destroy();
				}
			});
		}
		step++;
		pics.push(canvas.toDataURL());
		if(pics.length>step) step=pics.length;
	}, false);
	
}

var onPaint = function() {
	if(Click===1){
		ctx.strokeStyle='white';
	}
    ctx.lineTo(mouse.x, mouse.y);
	ctx.stroke();
	ctx.strokeStyle=colorElement.innerHTML;
};

var starting_pic;
var drawLine = function(){
	ctx.putImageData(starting_pic, 0, 0);
	ctx.beginPath();
	ctx.moveTo(mouse.start_x, mouse.start_y);
	ctx.lineTo(mouse.x, mouse.y);
	ctx.stroke();
}
var drawRectangle = function(){
	ctx.putImageData(starting_pic, 0, 0);
	ctx.beginPath();
	ctx.moveTo(mouse.start_x, mouse.start_y);
	ctx.lineTo(mouse.start_x, mouse.y);
	ctx.stroke();
	ctx.lineTo(mouse.x, mouse.y);
	ctx.stroke();
	ctx.lineTo(mouse.x, mouse.start_y);
	ctx.stroke();
	ctx.lineTo(mouse.start_x, mouse.start_y);
	ctx.stroke();
}
var drawTriangle = function(){
	ctx.putImageData(starting_pic, 0, 0);
	ctx.beginPath();
	ctx.moveTo((mouse.x + mouse.start_x)/2, mouse.start_y);
	ctx.lineTo(mouse.start_x, mouse.y);
	ctx.stroke();
	ctx.lineTo(mouse.x, mouse.y);
	ctx.stroke();
	ctx.lineTo((mouse.x + mouse.start_x)/2, mouse.start_y);
	ctx.stroke();
}
var drawCircle = function(){
	ctx.putImageData(starting_pic, 0, 0);
	ctx.beginPath();
	ctx.arc(mouse.start_x, mouse.start_y, Math.abs(mouse.x - mouse.start_x), 0, 2* Math.PI);
	ctx.stroke();
}
var createText = function(){
	ctx.putImageData(starting_pic, 0, 0);
	ctx.lineWidth = '5';
	ctx.beginPath();
	ctx.moveTo(mouse.start_x, mouse.start_y);
	ctx.lineTo(mouse.start_x, mouse.y);
	ctx.stroke();
	ctx.lineTo(mouse.x, mouse.y);
	ctx.stroke();
	ctx.lineTo(mouse.x, mouse.start_y);
	ctx.stroke();
	ctx.lineTo(mouse.start_x, mouse.start_y);
	ctx.stroke();
}
function convertHex(hex, opacity){
    hex = hex.replace('#','');
    r = parseInt(hex.substring(0,2), 16);
    g = parseInt(hex.substring(2,4), 16);
    b = parseInt(hex.substring(4,6), 16);

	if(!isNaN(opacity))
		result = 'rgba(' + r + ', ' + g + ', ' + b + ', ' + opacity/10 + ')';
	else
		result = 'rgb(' + r + ', ' + g + ', ' + b + ', ' + ')';
    return result;
}